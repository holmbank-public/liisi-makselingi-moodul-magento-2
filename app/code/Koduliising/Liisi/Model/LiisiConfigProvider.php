<?php

namespace Koduliising\Liisi\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Asset\Repository;

class LiisiConfigProvider implements ConfigProviderInterface
{
    /**
     * @var Repository
     */
    protected $_assetRepo;

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * Construct function.
     *
     * @param Repository $assetRepo
     * @param UrlInterface $urlBuilder
     */
    public function __construct(Repository $assetRepo, UrlInterface $urlBuilder)
    {
        $this->_assetRepo = $assetRepo;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $config = [
            'payment' => [
                'liisi' => [
                    'paymentAcceptanceMarkSrc' => $this->_assetRepo->getUrl('Koduliising_Liisi::images/logo.png'),
                    'redirectUrl' => $this->urlBuilder->getUrl('liisi/payment/start'),
                ],
            ],
        ];

        return $config;
    }
}
