<?php

namespace Koduliising\Liisi\Block\Payment;

use Magento\Checkout\Model\Session;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Koduliising\Liisi\Model\LiisiPaymentFactory;

class Start extends Template
{
    /**
     * @var LiisiPaymentFactory
     */
    private $liisiPaymentFactory;

    /**
     * @var Session
     */
    private $session;

    /**
     * Construct function.
     *
     * @param Context $context
     * @param LiisiPaymentFactory $liisiPaymentFactory
     * @param Session $session
     * @param array $data
     */
    public function __construct(
        Context $context,
        LiisiPaymentFactory $liisiPaymentFactory,
        Session $session,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->liisiPaymentFactory = $liisiPaymentFactory;
        $this->session = $session;
    }

    /**
     * @return string
     */
    public function getFormActionUrl()
    {
        $payment = $this->liisiPaymentFactory->create();
        $payment->getLiisiAPI();

        return $payment->getUrl();
    }

    /**
     * @return array
     */
    public function getFormFields()
    {
        return $this->liisiPaymentFactory->create()->getFormFields(
            $this->session->getLastRealOrder(),
            $this->getUrl('liisi/payment/index')
        );
    }
}
