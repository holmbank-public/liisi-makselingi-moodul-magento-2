/* global define */
define(['uiComponent', 'Magento_Checkout/js/model/payment/renderer-list'], function (Component, rendererList) {
        'use strict';

        rendererList.push({
            type: 'liisipayment',
            component: 'Koduliising_Liisi/js/view/payment/method-renderer/liisipayment-method'
        });

        return Component;
    }
);
