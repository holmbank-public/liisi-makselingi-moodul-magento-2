/* global define */
define([
    'Magento_Checkout/js/view/payment/default',
    'Magento_Checkout/js/action/redirect-on-success'
], function (Component, redirectOnSuccessAction) {
    'use strict';

    return Component.extend({
        redirectAfterPlaceOrder: false,

        defaults: {
            template: 'Koduliising_Liisi/payment/liisipayment'
        },

        /**
         * After place order callback
         */
        afterPlaceOrder: function () {
            redirectOnSuccessAction.redirectUrl = window.checkoutConfig.payment.liisi.redirectUrl;
            redirectOnSuccessAction.execute();
        }
    });
});


